angular.module('moduleExample', [])
    .controller('UserController', function ($scope) {

      // TODO 1 - přiřaďte data a funkce místo $scope do this
      $scope.user = {
        name: 'Petr',
        surname: 'Novák',
        yearOfBirth: 1982
      };

      $scope.getAge = function () {
        var now = new Date();
        return now.getFullYear() - $scope.user.yearOfBirth;
      };
    });
