describe('calculator controller', function() {
  var controller, calculatorMock;

  beforeEach(module('diApp'));

  beforeEach(inject(function($controller) {

    calculatorMock = {
      getPrice: function(product) {
      }
    };

    spyOn(calculatorMock, 'getPrice').and.returnValue(999);

    controller = $controller('CalculatorCtrl', {"calculator": calculatorMock});

  }));

  it('should call getPrice on factory with default product if getPrice is called', function() {
    controller.getPrice();
    expect(calculatorMock.getPrice).toHaveBeenCalledWith(controller.product);
    expect(calculatorMock.getPrice.calls.count()).toEqual(1);
  });

  it('should return value from calculator factory', function() {
    expect(controller.getPrice()).toEqual(999);
  });

});