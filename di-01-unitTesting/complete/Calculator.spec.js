describe('calculator', function() {

  beforeEach(module('diApp'));

  var loggerMock;

  beforeEach(function() {
    loggerMock = {
      log: function(message) {
      }
    };

    spyOn(loggerMock, 'log');

    module({logger:loggerMock});
  });

  it('should call logger', function() {
    inject(function(calculator) {
      calculator.getPrice({});
      expect(loggerMock.log).toHaveBeenCalled();
    });
  });

  it('should return 90 when format is A5 and number of pages is 0', function() {
    inject(function(calculator) {

    var product = {
      pageSize: 'A5',
      numberOfPages: 0
    };

    expect(calculator.getPrice(product)).toEqual(90);
    });
  });

  it('should return 120 when format is A5 and number of pages is 50', function() {
    inject(function(calculator) {

    var product = {
      pageSize: 'A5',
      numberOfPages: 50
    };

    expect(calculator.getPrice(product)).toEqual(120);
    });
  });

});