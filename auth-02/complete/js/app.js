angular.module('authApp', ['ngRoute', 'ngResource', 'ui.bootstrap'])
  .constant('REST_URI', 'http://security-api.angular.cz')

  .config(function ($routeProvider) {

    $routeProvider

      .when('/', {
        templateUrl: 'orderList.html',
        controller: 'OrderListController',
        controllerAs: 'list',
        resolve: {
          ordersData: function (Orders) {
            return Orders.query().$promise;
          }
        }
      })

      .when('/detail/:id', {
        templateUrl: 'orderDetail.html',
        controller: 'OrderDetailController',
        controllerAs: 'detail',
        resolve: {
          orderData: function (Orders, $route) {
            var id = $route.current.params.id;

            return Orders.get({'id': id}).$promise;
          }
        }
      })

      .when('/create', {
        templateUrl: 'orderCreate.html',
        controller: 'OrderCreateController',
        controllerAs: 'create',
        resolve: {
          orderData: function (Orders) {
            return new Orders();
          },
          isAuthorized: function (authService, loginModal) {
            return authService.isAuthenticated() || loginModal.showLoginModal();
          }
        }
      })

      .otherwise('/');
  })

  .factory("authInterceptor", function ($injector, $rootScope, REST_URI, $q) {

    return {
      responseError: function (response) {
        return $injector.invoke(function (loginModal, $http) {

          if (isRecoverable(response)) {
            return loginModal.showLoginModal()
              .then(function () {
                return $http(response.config);
              });
          }

          broadcastError(response);
          return $q.reject(response);
        });
      }
    };

    function isRecoverable(response) {
      return response.status === 401 && !isLoginPage(response);
    }

    function broadcastError(response) {
      if (isLoginPage(response)) {
        $rootScope.$broadcast('restApi:loginFailed');
      } else {
        $rootScope.$broadcast('restApi:forbidden');
      }
    }

    function isLoginPage(response) {
      return response.config.url === REST_URI + '/login'
    }
  })

  .factory("tokenInterceptor", function ($injector) {
    return {
      request: function (config) {
        var authService = $injector.get('authService');

        if (authService.isAuthenticated()) {
          config.headers = config.headers || {};
          config.headers['X-Auth-Token'] = authService.getToken();
        }

        return config;
      }
    };
  })

  .config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
    $httpProvider.interceptors.push('tokenInterceptor');
  })

  .run(function ($rootScope, $location) {
    $rootScope.$on('login:loggedOut', function () {
      $location.path("/")
    });
  })

  .run(function ($rootScope, $location) {
    $rootScope.$on('$routeChangeError', function () {
      $location.path('/');
    });
  })

  .run(function ($rootScope, loginModal) {
    $rootScope.$on('restApi:forbidden', function () {
      return loginModal.showRejectModal();
    });
  });


