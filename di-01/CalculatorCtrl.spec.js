describe('calculator controller', function() {
  var controller;

  beforeEach(module('diApp'));

  beforeEach(inject(function($controller) {

    controller = $controller('CalculatorCtrl', {});

  }));

  it('should return 90 when format is A5 and number of pages is 0', function() {

    controller.product = {
      pageSize: 'A5',
      numberOfPages: 0
    };

    expect(controller.getPrice()).toEqual(90);

  });

  it('should return 120 when format is A5 and number of pages is 50', function() {
    controller.product = {
      pageSize: 'A5',
      numberOfPages: 50
    };

    expect(controller.getPrice()).toEqual(120);
  });

});