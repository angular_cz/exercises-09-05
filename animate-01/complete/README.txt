Animate-01

Pro ukázku funkce animací je použit app_slow.css se zpomalenými animacemi.
Při přehrávání animací si všimněte, jak angular mění css třídy

1. ng-if 
  - podívejte se na css .type
  - využívá app.css a má definovány jen vstupní události 
  - použijte třídu .type na oba bloky ng-if
  - kód najdete v reality.html
  - vyzkoušejte změnu typu pozemku

2. ng-repeat 
  - podívejte se na css .cities
  - použijte třídu .cities na element s ng-repeat
  - zkuste hledání lokalit

3. ng-switch jako slider
  - podívejte se na css .information
  - použijte třídu .information na elementy ng-switch-when
  - vyzkoušejte jak se chová navigace ve slideru

4. ng-class
  - podívejte se na css .btn-animated-info
  - nahraďte třídu .btn-info za btn-animated-info u tlačítek nad sliderem

5. ng-form
  - podívejte se na css .validForm, které definují vzhled pro ng-invalid
  - přidejte třídu .validForm na oba elementy s třídou form-inline, nebo na nadřazený element v reality.html
  - vyzkoušejte zadání nečíselné hodnoty do rozlohy pozemku

6. ng-view
  - podívejte se na css .page
  - použijte třídu .page na ng-view v index.html
  - vyzkoušejte stisk tlačítka odeslat a poté akce zpět a vpřed v prohlížeči

7. Změňte css z app_slow.css na app.css